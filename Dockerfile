FROM opensuse/leap:15.6

RUN zypper ref --build-only && \
    zypper -n in rdiff-backup openssh-clients procps python3-setuptools && \
    zypper clean --all
